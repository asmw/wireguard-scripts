#!/bin/bash
set -e
set -u
DNS=${DNS_SERVER:-10.12.13.14}
SERVER=$(hostname)
echo "Using server hostname: $SERVER"
wg=${WG_COMMAND:-sudo wg}

INTERFACE=$($wg show interfaces | head -n1)
echo "Using interface: $INTERFACE"

LARGEST_ALLOWED=$($wg show $INTERFACE | grep "allowed ips" | awk -F: '{print $2}' | tr -d " " | sort -n | tail -n 1 | awk -F/ '{print $1}')
echo "Last peer IP: $LARGEST_ALLOWED"

echo "Generating keys..."
PRIVKEY=$(wg genkey)
PUBKEY=$(echo $PRIVKEY | wg pubkey)
SERVER_PUBKEY=$($wg show $INTERFACE public-key)
SERVER_PORT=$($wg show $INTERFACE listen-port)

NEXT_PEER=$(echo $LARGEST_ALLOWED | awk -F. '{print $1"."$2"."$3"."$4+1}')
mkdir $NEXT_PEER

echo "Using new peer IP: $NEXT_PEER"
echo "Client public key: $PUBKEY"
sed -e "s#%CLIENTADDRESS%#$NEXT_PEER/32#g" \
    -e "s#%CLIENTPRIVKEY%#$PRIVKEY#g" \
    -e "s#%CLIENTPUBKEY%#$PUBKEY#g" \
    -e "s#%SERVERPUBKEY%#$SERVER_PUBKEY#g" \
    -e "s/%SERVERADDRESS%/$SERVER:$SERVER_PORT/g" \
    -e "s/%DNS%/$DNS/g" \
    wgpeer.conf.template > $NEXT_PEER/wgpeer.conf


echo "$wg set $INTERFACE peer $PUBKEY allowed-ips $NEXT_PEER/32" > $NEXT_PEER/install.sh
echo "$wg set $INTERFACE peer $PUBKEY remove" > $NEXT_PEER/remove.sh
echo "qrencode -t ansiutf8 < $PWD/$NEXT_PEER/wgpeer.conf" > $NEXT_PEER/qrcode.sh
chmod +x $NEXT_PEER/*.sh
echo "Setup scripts created in $PWD/$NEXT_PEER"
