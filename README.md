# Wireguard helper scripts

This set of scripts is particular to my setup.

Usage:
DNS_SERVER=my.dns.server ./add_peer.sh

## Assumptions:
- Peers have a `a.b.c.d./32` address and we can just increase `d` to get the next address
- The default DNS server to use is 10.12.13.14

## TODO:
- Make up a private network without any existing peers
- IPv6 stuff
